---
sidebar_position: 3
---

# Hoppr Docker Image - Intermediate

This tutorial will show you how to run Hoppr from a Docker image.

## Getting started

To download the `hoppr/hopctl` image run `docker pull hoppr/hopctl`.

Reference:

- Dockerfile: <https://gitlab.com/hoppr/hoppr/-/blob/dev/docker/release/Dockerfile>
- Hopctl tags: <https://hub.docker.com/r/hoppr/hopctl/tags>

## Running the Bundle Command

To run Hoppr using Docker run, you can use the `hoppr/hopctl` image.

Example of how to run `hopctl` subcommands:

```sh
docker run \
hoppr/hopctl <subcommand> <arguments>
```

If you want to run a `hopctl` subcommand in a local directory, you can mount that directory into a volume using the `-v` or `--volume` option.

Example mounting the current working directory as a volume:

```sh
docker run -v $(pwd):/data -w /data \
hoppr/hopctl <subcommand> <arguments>
```

Example bundle where `transfer.yml`, `manifest.yml` and the SBOM file referenced in the manifest file are all in the current working directory:

```sh
docker run -v $(pwd):/data -w /data  \
hoppr/hopctl bundle manifest.yml --transfer transfer.yml
```

You may need to set environment variables for things like credentials, proxies, certs, ect...

Example with an environment variable:

```sh
docker run -v $(pwd):/data -w /data \
-e http_proxy=http://myproxy.mydomain.com \
hoppr/hopctl <subcommand> <arguments>
```
