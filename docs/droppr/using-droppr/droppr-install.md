---
sidebar_position: 1
title: Droppr Installation
description: How to install Droppr
---

# Installation

Go to [Droppr Releases](https://gitlab.com/hoppr/droppr/-/releases) and download the appropriate binary for your system.

Droppr should be downloaded and installed onto the target system to be used.

Once the Hoppr bundle is on the target system and Droppr is installed on the target system one can run Droppr.

Example of Installing Downloaded Binary
```
mv droppr /usr/local/bin
chmod +x /usr/local/bin/droppr
```

One can also append the Binary to the PATH if they choose to
```
export $PATH=$PATH.":/path/to/"
chmod +x /path/to/droppr
```
